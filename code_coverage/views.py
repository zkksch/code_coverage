from django.utils import timezone
from time import *
import datetime
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.utils.decorators import method_decorator
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, Http404
from django.shortcuts import render
from django.utils.timezone import utc
from django.views.generic import View
from django.views.generic.base import TemplateResponseMixin
from django.views.generic.edit import FormMixin
from models import *
from models import Commit as DBCommit, Actor as DBActor, File as DBFile
from forms import *
from git import *
import subprocess
from xml.etree import ElementTree
import os
import re
import shutil


class IndexView(View,TemplateResponseMixin,FormMixin):
    template_name = "code_coverage/index.html"

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context["repos"] = Repository.objects.filter(user = self.request.user)
        return context

    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        return self.render_to_response(self.get_context_data(), **kwargs)


class RepoStatView(View,TemplateResponseMixin,FormMixin):
    template_name = "code_coverage/stats.html"

    def get_context_data(self, repo_id, **kwargs):
        context = super(RepoStatView, self).get_context_data(**kwargs)
        context['branches'] = []
        for branch in Branch.objects.filter(repository=repo_id):
            b = {}
            b['name'] = branch.name
            b['commits'] = []
            for commit in DBCommit.objects.filter(branch=branch):
                c = {}
                c['sha'] = commit.sha
                c['coverage'] = commit.coverage
                c['actors'] = []
                c['files'] = []
                for file in DBFile.objects.filter(commit=commit):
                    f = {}
                    f['path'] = file.path_to_file
                    f['coverage'] = file.coverage
                    c['files'].append(f)
                for actor in ActorStats.objects.filter(commit=commit):
                    a = {}
                    a['name'] = actor.actor.actor_name
                    a['coverage'] = actor.coverage
                    c['actors'].append(a)
                b['commits'].append(c)
            context['branches'].append(b)
        return context

    @method_decorator(login_required)
    def get(self, request, repo_id, *args, **kwargs):
        return self.render_to_response(self.get_context_data(repo_id=repo_id), **kwargs)


class DownloaderView(View, TemplateResponseMixin, FormMixin):
    template_name = "code_coverage/downloader.html"
    form_class = RepositoryForm

    def get_context_data(self, message, **kwargs):
        context = super(DownloaderView, self).get_context_data(**kwargs)
        context["message"] = message
        return context

    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        return self.render_to_response(self.get_context_data(message=None), **kwargs)

    @method_decorator(login_required)
    def post(self, request, *args, **kwargs):
        form = self.get_form(self.get_form_class())
        if form.is_valid():
            return self.form_valid(form, request, **kwargs)
        else:
            return self.form_invalid(form, **kwargs)

    def form_valid(self, form, request, **kwargs):
        url = form.cleaned_data["url"]
        try:
            r = Repository(url=url, user=request.user)
            r.save()
            Repo.clone_from(url, "cache/%s" % str(r.id))
            path = "cache/%s" % str(r.id)
            g = Git(path)
            repo = Repo(path)
            for head in repo.heads:
                head.checkout()
                head_commit = head.commit
                branch_commits = [head_commit]
                for c in head_commit.iter_parents():
                    branch_commits.append(c)
                if not Branch.objects.filter(repository=r, name=head.name):
                    b = Branch(repository=r, name=head.name)
                    b.save()
                else:
                    b = Branch.objects.get(repository=r, name=head.name)
                for commit in reversed(branch_commits):
                    g.execute('git checkout %s' % commit.hexsha)
                    if not DBCommit.objects.filter(branch=b, sha=commit.hexsha):
                        if not DBActor.objects.filter(repository=r, actor_name=commit.author.name, actor_email=commit.author.email):
                            a = DBActor(repository=r, actor_name=commit.author.name, actor_email=commit.author.email)
                            a.save()
                        else:
                            a = DBActor.objects.get(repository=r, actor_name=commit.author.name, actor_email=commit.author.email)
                        c = DBCommit(branch=b, actor=a, committed_date=datetime.datetime.fromtimestamp(commit.committed_date).date(), sha=commit.hexsha)
                        c.save()
                    else:
                        c = DBCommit.objects.get(branch=b, sha=commit.hexsha)
                    files = self.walk(g, commit.tree)
                    for file in files:
                        if not DBFile.objects.filter(commit=c, path_to_file=file):
                            f = DBFile(commit=c, path_to_file=file)
                            f.save()
                        else:
                            f = DBFile.objects.get(commit=c, path_to_file=file)
                        count = 0
                        path = file.split(':')
                        path = "\\".join(path)
                        white_regex = re.compile('^\s*$')
                        first_line = True
                        last_line = False
                        for line in g.execute('git blame --line-porcelain "%s"' % path).split('\n'):
                            count += 1
                            if first_line:
                                first_line = False
                                if white_regex.match(line) is None:
                                    cur_com = DBCommit.objects.get(branch=b, sha=line.split(' ')[0])
                                    cur_num = line.split(' ')[2]
                            elif last_line:
                                first_line = True
                                if white_regex.match(line) is None:
                                    if not Line.objects.filter(file=f, actor=cur_com.actor, num=cur_num):
                                        l = Line(file=f, actor=cur_com.actor, num=cur_num)
                                        l.save()
                            if line.split(" ")[0] == 'filename' and not last_line:
                                last_line = True
                            else:
                                last_line = False

            commits = []
            for head in repo.heads:
                commits.append({'commit': head.commit, 'head': head})
                for c in head.commit.iter_parents():
                    commits.append({'commit': c, 'head': head})
            context = {'commits': commits, 'repo_id': r.id}
            return render(request, 'code_coverage/selector.html', context)

        except GitCommandError as ex:
            message = ex

        except InvalidGitRepositoryError:
            message = "Invalid repository"

        return self.render_to_response(self.get_context_data(message=message), **kwargs)

    def form_invalid(self, form, **kwargs):
        return self.render_to_response(self.get_context_data(message="Wrong url!"), **kwargs)

    def walk(self, g, tree):
        pyfile = re.compile('.*\.py$')
        files = []
        trees = []
        trees.append(tree)
        while len(trees)>0:
            cur_tree = trees.pop()
            for t in cur_tree.trees:
                trees.append(t)
            for b in cur_tree.blobs:
                if pyfile.match(str(b.abspath)):
                    path = b.abspath.split('\\')
                    path = path[path.index('cache')+2:]
                    path = ":".join(path)
                    files.append(path)
        return files


class SelectorView(View, TemplateResponseMixin, FormMixin):
    template_name = 'code_coverage/selector.html'

    def get_context_data(self, **kwargs):
        context = super(SelectorView, self).get_context_data(**kwargs)
        context = kwargs
        return context

    @method_decorator(login_required)
    def post(self, request, *args, **kwargs):
        r = Repository.objects.get(id=request.POST['repo_id'])
        path = "cache/%s" % str(r.id)
        repo = Repo(path)
        g = Git(path)
        files = []
        prev_test_file = ""
        for branch in repo.heads:
            b = Branch.objects.get(name=branch.name, repository=r)
            branch.checkout()
            head_commit = branch.commit
            branch_commits = [head_commit]
            for c in head_commit.iter_parents():
                branch_commits.append(c)
            for c in branch_commits:
                commit_cov = 0
                com = DBCommit.objects.get(branch=b, sha=c.hexsha)
                actors = {}
                for act in DBActor.objects.filter(repository=r):
                    actors[act.id] = 0
                if c.hexsha in request.POST:
                        if request.POST[c.hexsha] != "":
                            test_file = request.POST[c.hexsha]
                            prev_test_file = test_file
                        else:
                            test_file = prev_test_file
                g.execute('git checkout %s' % c.hexsha)
                try:
                    for test_path in test_file.split(';'):
                        if request.POST['test_type'] == 'pytest':
                            subprocess.call(['coverage', 'run', "-m", "pytest", '{0}/{1}'.format(path, test_path)])
                        #elif request.POST['test_type'] == 'django':
                        #    subprocess.call(['coverage', 'run', '{0}/{1}'.format(path, 'manage.py'), 'test'])
                        else:
                            subprocess.call(['coverage', 'run', '{0}/{1}'.format(path, test_path)])
                        subprocess.call(['coverage', 'xml', '-o', '%s/coverage.xml' % path])
                        t = ElementTree.parse('%s/coverage.xml' % path)
                        cov = t.getroot()
                        for child in cov[0][0][0]:
                            file_path = child.attrib["filename"].split('/')
                            file_path = file_path[file_path.index(str(r.id))+1:]
                            f = DBFile.objects.get(commit=com, path_to_file=file_path[0])
                            f.coverage = float(child.attrib["line-rate"])
                            commit_cov += float(child.attrib["line-rate"])
                            f.save()
                            files.append(c.hexsha + " : " + f.path_to_file + " : " + child.attrib["line-rate"])
                            for line in child[1]:
                                l = Line.objects.get(file=f, num=line.attrib["number"])
                                if line.attrib["hits"] == '1':
                                    actors[l.actor.id] += 1
                                    l.coverage = True
                                    l.save()
                except Exception as ex:
                    files.append(c.hexsha + " : " + str(ex))
                try:
                    commit_cov /= len(File.objects.filter(commit=com))
                except:
                    commit_cov = 0
                com.coverage = commit_cov
                com.save()
                for act in DBActor.objects.filter(repository=r):
                    total_lines = 0
                    for file in File.objects.filter(commit=com):
                        for line in Line.objects.filter(file=file):
                            if line.actor == act:
                                total_lines += 1
                    if ActorStats.objects.filter(actor=act, commit=com):
                        astat = ActorStats.objects.get(actor=act, commit=com)
                        try:
                            astat.coverage = 1.0*actors[act.id]/total_lines
                            astat.save()
                        except:
                            pass
                    else:
                        try:
                            astat = ActorStats(actor=act, commit=com, coverage=1.0*actors[act.id]/total_lines)
                            astat.save()
                        except:
                            pass
                subprocess.call(['coverage', 'erase'])
            try:
                os.remove('%s/coverage.xml' % path)
            except:
                pass
        context = {'files': files}
        #shutil.rmtree(path, ignore_errors=True, onerror=None)
        return HttpResponseRedirect(str(r.id))


class RegisterView(View, TemplateResponseMixin, FormMixin):
    template_name = "code_coverage/register.html"

    def get_context_data(self, error, **kwargs):
        context = super(RegisterView,self).get_context_data(**kwargs)
        context["error_message"] = error
        return context

    def get(self, request, *args, **kwargs):
        if request.user.is_anonymous():
            return render(request, 'code_coverage/register.html')
        else:
            return HttpResponseRedirect(reverse('index'))

    def post(self, request, *args, **kwargs):
        username = request.POST["username"]
        password = request.POST["password"]
        password2 = request.POST["password2"]
        email = request.POST["email"]
        u = User.objects.filter(username=username)
        if u:
            return self.render_to_response(self.get_context_data(error="Choose another username"),**kwargs)
        u = User.objects.filter(email=email)
        if u:
            return self.render_to_response(self.get_context_data(error="Choose another email"),**kwargs)
        if password != password2:
            return self.render_to_response(self.get_context_data(error="Passwords do not match"),**kwargs)
        u = User.objects.create_user(username=username, email=email, password=password)
        u.save()
        log = LoginView()
        return log.post(request=self.request)

class LoginView(View, TemplateResponseMixin, FormMixin):
    template_name = "code_coverage/login.html"

    def get_context_data(self, error, **kwargs):
        context = super(RegisterView, self).get_context_data(**kwargs)
        context["error_message"] = error
        return context

    def get(self, request, *args, **kwargs):
        if request.user.is_anonymous():
            if request.GET.has_key("next"):
                context = {'next': request.GET["next"]}
            else:
                context = {}
            return render(request, 'code_coverage/login.html', context)
        else:
            return HttpResponseRedirect(reverse('index'))

    def post(self, request, *args, **kwargs):
        username = request.POST["username"]
        password = request.POST["password"]
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            if "next" in request.GET:
                redirect_path = request.GET["next"]
            else:
                redirect_path = reverse('index')
            return HttpResponseRedirect(redirect_path)
        else:
            return HttpResponseRedirect(reverse('login'))


def logout_view(request):
    if request.user.is_authenticated():
        logout(request)
    return HttpResponseRedirect(reverse('login'))