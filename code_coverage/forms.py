from django import forms

class RepositoryForm (forms.Form):
    url = forms.CharField(max_length=256)